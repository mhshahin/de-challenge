import sys
import src
from src import constants
import logging


def main():
    # worker pool initializer
    pool = src.pool_init()

    # command-line arguments parser
    args = src.args_parser(sys.argv[1:])

    # checks if size of chunks provided
    if args.chunk:
        chunk_size = int(args.chunk)
    else:
        chunk_size = constants.DEFAULT_CHUNK_SIZE

    # checks for ip masking bits
    if args.ipv4mask and args.ipv6mask:
        ipv4_mask_bits = 32 - int(args.ipv4mask)
        ipv6_mask_bits = 128 - int(args.ipv6mask)
    else:
        ipv4_mask_bits = 16  # 32 - 16, mask 16 bits
        ipv6_mask_bits = 96  # 128 - 32, mask 32 bits

    # ip to country approach
    if args.database:
        geoip_db_path = args.database
        if args.input and args.multiprocess:
            log_path = args.input
            src.log_parser_ip_to_country_multiprocess(log_path=log_path, geoip_db_path=geoip_db_path,
                                                      pool=pool, chunk_size=chunk_size)
        elif args.input and not args.multiprocess:
            log_path = args.input
            src.log_parser_ip_to_country_single_process(log_path=log_path, geoip_db_path=geoip_db_path)

        elif not args.input:
            log_path = constants.FILE_INPUT_STDIN
            src.log_parser_ip_to_country_single_process(log_path=log_path, geoip_db_path=geoip_db_path)
        else:
            logging.error("Too few arguments.")

    # ip masking approach
    if not args.database:
        if args.multiprocess and args.input:
            log_path = args.input
            src.log_parser_mask_ip_multiprocess(log_path=log_path, ipv4_mask_bits=ipv4_mask_bits,
                                                ipv6_mask_bits=ipv6_mask_bits, chunk_size=chunk_size, pool=pool)
        elif not args.multiprocess and args.input:
            log_path = args.input
            src.log_parser_mask_ip_single_process(log_path=log_path, ipv4_mask_bits=ipv4_mask_bits,
                                                  ipv6_mask_bits=ipv6_mask_bits)
        elif not args.multiprocess and not args.input:
            log_path = constants.FILE_INPUT_STDIN
            src.log_parser_mask_ip_single_process(log_path=log_path, ipv4_mask_bits=ipv4_mask_bits,
                                                  ipv6_mask_bits=ipv6_mask_bits)
        else:
            logging.error("Too few arguments.")


if __name__ == "__main__":
    main()
