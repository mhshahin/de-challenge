import argparse


def args_parser(args):
    parser = argparse.ArgumentParser(
        description="A simple command-line utility to anonymize Apache Web Server's log PPI.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-ipv4mask",
        metavar="integer",
        help="Truncates the last n bits of the IPv4 (Defaults to 16 bits)",
    ),

    parser.add_argument(
        "-ipv6mask",
        metavar="integer",
        help="Truncates the last n bits of the IPv6 (Defaults to 32 bits",
    )

    parser.add_argument(
        "-i",
        "--input",
        metavar="file",
        help="File or FIFO to read from"
    )

    parser.add_argument(
        "-p",
        "--multiprocess",
        metavar="bool",
        help="Choose whether to process the file sequentially or using multiprocessing"
    )

    parser.add_argument(
        "-c",
        "--chunk",
        metavar="integer",
        help="Chunk size (Default: 100,000 lines)"
    )
    parser.set_defaults(chunk=100000)

    parser.add_argument(
        "-d",
        "--database",
        metavar="file",
        help="Path to GeoIP database with MDMM format"
    )

    args = parser.parse_args(args)

    return args
