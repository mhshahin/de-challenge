import multiprocessing as mp


def pool_init():
    """
    Initializes worker pool based on number of cpu logical cores.
    The initializer reserves at least 1 logical core for the system and dedicates the rest
    to the worker pool. This would prevent the machine to hang if the processing is timely.

    :return pool:
    """
    system_cpu = mp.cpu_count()
    if 1 < system_cpu <= 6:
        pool = mp.Pool(system_cpu - 1)
    else:
        pool = mp.Pool(system_cpu - 2)

    return pool
