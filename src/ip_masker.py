import ipaddress
import logging
from .ip_extractor import ip_extractor
from .ip_validator import ipv4_validator, ipv6_validator
import uuid


def ip_masker(log_file, out_log_dir, ipv4_mask_bits, ipv6_mask_bits, pattern, multiprocess=True):
    """
    Masks the IP address as much as the bits defined.

    :param log_file: path to the log file or chunk
    :param out_log_dir: path to the output anonymized file
    :param ipv4_mask_bits: number of bits to mask the IPv4
    :param ipv6_mask_bits: number of bits to mask the IPv6
    :param pattern: IPv4/IPv6 pattern
    :param multiprocess: whether to use multiprocessing or single-processing (Default is True)
    :return:
    """
    if multiprocess:
        output_file = "{}tmp-masker-{}.log".format(out_log_dir, str(uuid.uuid4()))
    else:
        output_file = "{}anon-access-log-masker.log".format(out_log_dir)

    with open(output_file, 'w') as f:
        for line in log_file:
            ip_address = ip_extractor(line, pattern)
            if ipv4_validator(ip_address):
                masked_ip = str(ipaddress.ip_network(ip_address).supernet(new_prefix=ipv4_mask_bits)[0])
                f.write(pattern.sub(masked_ip, line, 1))
            elif ipv6_validator(ip_address):
                masked_ip = str(ipaddress.ip_network(ip_address).supernet(new_prefix=ipv6_mask_bits)[0])
                f.write(pattern.sub(masked_ip, line, 1))
            else:
                logging.error("There was an error masking the IP address.")
