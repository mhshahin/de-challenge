IP_REG_PATTERN = "(?P<ip>[.:0-9a-fA-F]+)"
DEFAULT_CHUNK_SIZE = 10000
TMP_LOGS_DIR = 'log/'
FILE_INPUT_STDIN = "-" # fileinput module takes this as stdin
