import logging


def ip_extractor(log_line, pattern):
    """
    Looks for IP address pattern in given line from log file and returns the IP address.

    :param log_line: log line from the log file
    :param pattern: IPv4/IPv6 pattern
    :return: if there was an IP address in the log line, returns a string representation of the extracted IP address
    """
    try:
        return pattern.search(log_line).group()
    except Exception as e:
        logging.error("There was an error while extracting IP address: {}".format(e))
