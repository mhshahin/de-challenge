import logging
import os
from . import constants


def create_tmp_log_dir(current_path: str):
    """
    Creates a temporary "log" directory to write temporary anonymized log files in it.

    :param current_path: the path in which you run the project
    :return:
    """
    log_dir_path = current_path + constants.TMP_LOGS_DIR

    if not os.path.exists(log_dir_path):
        try:
            os.mkdir(log_dir_path)
        except OSError as e:
            logging.error("There was an error in creating temp directory. Error: {}".format(e))
    else:
        logging.error("Directory 'log' already exists.")

