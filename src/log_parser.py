import re
from . import constants
from itertools import islice
from .ip_to_country import ip_to_country
from .ip_masker import ip_masker
from fileinput import FileInput
import os
from .utils import create_tmp_log_dir

pattern = re.compile(constants.IP_REG_PATTERN)
current_dir = os.getcwd() + '/'


def log_parser_ip_to_country_single_process(log_path, geoip_db_path):
    """
    Single-process version of ip to country; in case of reading from sys.stdin, this function will be used
    to process the log file. Be advised that if you want to use process a very large file, do not use this function.
    It takes forever to process a large file using single-processing.

    :param log_path: path to the log file or standard input
    :param geoip_db_path: path to the GeoIP database
    :return:
    """
    with FileInput(log_path, inplace=False) as log_file:
        ip_to_country(log_file=log_file, geoip_db_path=geoip_db_path, pattern=pattern,
                      out_log_dir=current_dir, multiprocess=False)


def log_parser_ip_to_country_multiprocess(log_path, geoip_db_path, pool, chunk_size):
    """
    Multi-process version of ip to country. This is the preferred approach to process very large log files.
    This function will create a 'log' directory in current path and write multiple tmp files in it.

    :param log_path: path to the log file (can't be used with standard input)
    :param geoip_db_path: path to the GeoIP database
    :param pool: worker pool
    :param chunk_size: the size of each chunk of the log file in lines
    :return:
    """
    create_tmp_log_dir(current_path=current_dir)
    with FileInput(log_path, inplace=False) as log_file:
        while True:
            chunk = list(islice(log_file, chunk_size))
            if not chunk:
                break

            pool.starmap_async(ip_to_country, ((chunk, geoip_db_path, pattern, current_dir + constants.TMP_LOGS_DIR),))
    pool.close()
    pool.join()


def log_parser_mask_ip_single_process(log_path, ipv4_mask_bits, ipv6_mask_bits):
    """
    Single-process version of ip masker. This function masks the IP address according to the bits defined.
    It's the default method that will be used to process the standard input.

    :param log_path: path to the log file or standard input
    :param ipv4_mask_bits: number of bits to mask the IPv4
    :param ipv6_mask_bits: number of bits to mask the IPv6
    :return:
    """
    with FileInput(log_path, inplace=False) as log_file:
        ip_masker(log_file=log_file, out_log_dir=current_dir, ipv4_mask_bits=ipv4_mask_bits,
                  ipv6_mask_bits=ipv6_mask_bits, pattern=pattern, multiprocess=False)


def log_parser_mask_ip_multiprocess(log_path, ipv4_mask_bits, ipv6_mask_bits, chunk_size, pool):
    """
    Multi-process version of the ip masker. It will mask the IP address as much as the bits defined.

    :param log_path: path to the log file
    :param ipv4_mask_bits: number of bits to mask the IPv4
    :param ipv6_mask_bits: number of bits to mask the IPv6
    :param chunk_size: size of the log file chunks in lines
    :param pool: worker pool
    :return:
    """
    create_tmp_log_dir(current_path=current_dir)
    with FileInput(log_path, inplace=False) as log_file:
        while True:
            chunk = list(islice(log_file, chunk_size))
            if not chunk:
                break

            pool.starmap_async(ip_masker, ((chunk, current_dir + constants.TMP_LOGS_DIR, ipv4_mask_bits,
                                            ipv6_mask_bits, pattern),))
    pool.close()
    pool.join()
