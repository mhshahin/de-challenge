import ipaddress
import logging


def ipv4_validator(ipv4_address: str):
    """
    Checks whether ipv4_address is of IPv4 type or not
    :param ipv4_address:
    :return:
    """
    try:
        ipaddress.IPv4Address(ipv4_address)
        return True
    except Exception as e:
        logging.info("Provided value is not a valid IPv4 address. Error: {}".format(e))
        return False


def ipv6_validator(ipv6_address: str):
    """
    Checks whether ipv4_address is of IPv6 type or not
    :param ipv6_address:
    :return:
    """
    try:
        ipaddress.IPv6Address(ipv6_address)
        return True
    except Exception as e:
        logging.info("Provided value is not a valid IPv6 address. Error: {}".format(e))
        return False
