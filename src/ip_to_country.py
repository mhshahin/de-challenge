from .ip_extractor import ip_extractor
from .ip_validator import ipv4_validator, ipv6_validator
import logging
import uuid
import geoip2.database


def ip_to_country(log_file, geoip_db_path, pattern, out_log_dir, multiprocess=True):
    """
    Substitutes IP address with country name.

    :param log_file: path to the log file or chunk
    :param geoip_db_path: GeoIP database path
    :param pattern: IPv4/IPv6 pattern
    :param out_log_dir: anonymized log file path
    :param multiprocess: whether to use multiprocessing or no (default is True)
    :return:
    """
    if multiprocess:
        output_file = "{}tmp-country-{}.log".format(out_log_dir, str(uuid.uuid4()))
    else:
        output_file = "{}anon-access-log-country.log".format(out_log_dir)

    with geoip2.database.Reader(geoip_db_path) as db_reader:
        with open(output_file, 'w') as f:
            for line in log_file:
                ip_address = ip_extractor(line, pattern)
                # This line checks if the extracted IP address is valid; whether IPv4 or IPv6
                if ipv4_validator(ip_address) or ipv6_validator(ip_address):
                    country_name = get_country(db_reader, ip_address)
                    try:
                        f.write(pattern.sub(country_name, line, 1))
                    except Exception as e:
                        # logging.error("There was an error in writing replaced line to temp file with error: {}".format(e))
                        pass
                else:
                    logging.error("Extracted IP address is not valid.")


def get_country(db_reader, ip_address):
    """
    Maps users IP address with its corresponding country.

    :param db_reader:
    :param ip_address:
    :return:
    """
    try:
        response = db_reader.country(ip_address)
        return response.country.name
    except Exception as e:
        # logging.error("There was an error in getting origin country of the IP address. Error: {}".format(e))
        return 'N/A'
