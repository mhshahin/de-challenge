# Apache Log Anonymizer

### Setup
Use `requirements.txt` to install the required modules.

### Usage:

Note: The path to files should be absolute.

Whenever you want to anonymize a log file, you have the option to select the size of chunks in lines.
For example, if the log file has 15M lines, use chunks of 1M lines:
```bash
-c 1000000
```
For bigger files, use bigger chunk sizes. This will help with processing time.

#### IP masking a log file using multiprocessing:
```bash
python3 main.py -i <log_file> -ipv4mask <bits> -ipv6mask <bits> -p true 
```

#### IP masking from `stdin`:
```bash
python3 main.py -ipv4mask <bits> -ipv6mask <bits>
```

#### IP to Country a log file using multiprocessing:
```bash
python3 main.py -i <log_file> -d <geo_ip_db> -p true 
```

#### IP to Country from `stdin`:
```bash
python3 main.py -d <geo_ip_db>
```